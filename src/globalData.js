/*
 * @Author: zhaohui yuan
 * @Date: 2023-01-05 22:58:40
 * @Purpose: 常用配置字段
 */

export default {
  title: "最佳拍档",
  color: "#3b79fe",
  version: "PROD",
  default_remote: "https://prod.openea.net",
  Remote_Config: [
	  {system: "sso", remote: "https://sso.openea.net"},
	  {system: "rest", remote: "https://jl.openea.net"},
	  {system: "crm", remote: "https://crm.openea.net"},
	  {system: "wxpay", remote: "https://wxpay.openea.net"},
	  {system: "payment", remote: "https://pay.openea.net"},
	  {system: "comm", remote: "https://comm.openea.net"},
	  {system: "meily", remote: "https://meily.openea.net"}
  ],
  
  baseURL: "https://www.openea.net/",
  adoneURL: "https://www.openea.net/linkey/bpm/appfile/AppletsPro/adone.jpg",
  adtwoURL: "https://www.openea.net/linkey/bpm/appfile/AppletsPro/adtwo.jpg",
  adthreeURL: "https://www.openea.net/linkey/bpm/appfile/AppletsPro/adthree.jpg",
  
  codeURL: "https://www.openea.net/linkey/bpm/appfile/AppletsPro/code.png",
  QRcodeURL: 'https://www.openea.net/linkey/bpm/appfile/AppletsPro/QRcode.jpg',
  adminHead: "https://www.openea.net/linkey/bpm/appfile/AppletsPro/adminHead.jpg",
  inviteURL: "https://www-1308272144.cos.ap-guangzhou.myqcloud.com/Meily/img/invite.png",
  iconURL: "https://www-1308272144.cos.ap-guangzhou.myqcloud.com/Meily/icon/",
  imgURL: "https://www-1308272144.cos.ap-guangzhou.myqcloud.com/Meily/img/",
  qrURL: "https://www-1308272144.cos.ap-guangzhou.myqcloud.com/static/qr/",
  customerService: "https://www-1308272144.cos.ap-guangzhou.myqcloud.com/static/image/20221128225200.jpg",//客服微信二维码图片地址
  
  ICON: {
		personalIndex1: "https://www-1308272144.cos.ap-guangzhou.myqcloud.com/Meily/icon/personalIndex1.png",
		personalIndex2: "https://www-1308272144.cos.ap-guangzhou.myqcloud.com/Meily/icon/personalIndex2.png",
		personalIndex3: "https://www-1308272144.cos.ap-guangzhou.myqcloud.com/Meily/icon/personalIndex3.png",
		personalIndex4: "https://www-1308272144.cos.ap-guangzhou.myqcloud.com/Meily/icon/personalIndex4.png",
		personalIndex5: "https://dev-1308272144.cos.ap-guangzhou.myqcloud.com/Meily/icon/personalIndex5.png",
		personalIndex6: "https://dev-1308272144.cos.ap-guangzhou.myqcloud.com/Meily/icon/personalIndex6.png",
		personalIndex7: "https://dev-1308272144.cos.ap-guangzhou.myqcloud.com/Meily/icon/personalIndex7.png"
  },
  IMGS: {
  	  BoyAvatar: "https://www-1308272144.cos.ap-guangzhou.myqcloud.com/Meily/img/boy.png",
  	  GirlAvatar: "https://www-1308272144.cos.ap-guangzhou.myqcloud.com/Meily/img/girl.png",
	  DefaultServiceProviderAvatara: "https://www-1308272144.cos.ap-guangzhou.myqcloud.com/Meily/img/DefaultServiceProviderAvatara.jpg"
  },
  //过滤用户使用的默认头像地址
  FilterDefaultAvatar: [
  	  "https://mmbiz.qpic.cn/mmbiz/icTdbqWNOwNRna42FI242Lcia07jQodd2FJGIYQfG0LAJGFxM4FbnQP6yfMxBgJ0F3YRqJCJ1aPAK2dQagdusBZg/0",
  	  "https://thirdwx.qlogo.cn/mmopen/vi_32/POgEwh4mIHO4nibH0KlMECNjjGxQUq24ZEaGT4poC6icRiccVGKSyXwibcPq4BWmiaIGuG1icwxaQX6grC9VemZoJ8rg/132"
  ],
  envVersion: "release",
  
  CosSecretId: "AKIDfATcXDdpmW3LOCl48xWptjKflcDZgYAY",
  CosSecretKey: "B6mDOK1TggSzGI4buynMgkY8SXMvFK9j",
  CosBucket: "www-1308272144",
  CosRegion: "ap-guangzhou",
  
  authsalt: "WxPerPartner" ,//支付授权盐
  
  corpId:'wwf9c482f0215bc29d', // 企业id
  
  // 微信客服id
  openKfid_platform: 'wkj8ajbwAAMfcKyiivK8fUZg6rjXu5Dw',// 玫丽平台
  openKfid_learning: 'wkj8ajbwAAJ9CA0fP5dAF1gh8yBdzvfw',// 玫丽学习
  openKfid_preSales: 'wkj8ajbwAAOWn1TIhcmm7ExhRqujvh4g', // 玫丽生活售前
  openKfid_afterSales: 'wkj8ajbwAAMjw-9m6IWPQZm9VOdCJn3g', // 玫丽生活售后
  openKfid_evaluation: 'wkj8ajbwAAisscz9CsLIdBix5k_5JgKg', // 玫丽测评
  openKfid_sample: 'wkj8ajbwAAN0wtVUhAufcOsh9GNTb7nQ', // 玫丽样品
  openKfid_HR: 'wkj8ajbwAAMvjhXMXrahFaov7HFuKMHg', // 玫丽直聘
  openKfid_welfare: 'wkj8ajbwAAW7C9gj34M6gBQQFDBYvMVA' ,//玫丽福利官
  openKfid_customized: 'wkj8ajbwAAYypZGTXu1AD36G4TOI2aiw'//玫丽平台定制
};
