export default {
	
    data () {
        return {
            shareData: {
                docUnid: '',
				url: '',
				route: '',
				style: '',
				newId: '',
				btn: '',
				shareTitle: '',
				imageUrl: ''
            },
        }
    },
	
	onLoad(){
		wx.showShareMenu({
			 withShareTicket: true,
			 menus: ["shareAppMessage", "shareTimeline"]
		})
	},
	

	//uniapp微信小程序分享页面到微信好友
    onShareAppMessage () {
		return this.getTypeByRoute()
    },
	
	//uniapp微信小程序分享页面到微信朋友圈
	onShareTimeline(res) {
		return {
			title: this.sharedata.title,
			query: '',
			imageUrl: this.sharedata.imageurl,
			success(res) {
				uni.showToast({
					title: '分享成功'
				})
			},
			fail(res) {
				uni.showToast({
					title: '分享失败',
					icon: 'none'
				})
			}
		}
	},
	
	methods: {
		
		/**
		 * 通过路径获取分享类型
		 */
		async getTypeByRoute(){
			// let res = await this.$u.api.unifyRequest({
			// 	appid: 'AppletInterface',
			// 	wf_num: 'R_AppletInterface_BD01',
			// 	path: this.shareData.route,//类型
			// 	loading: true, //默认发起请求会有一个全局的Loading，设置false可去掉
			// });
			// if(res.code == 200){
				//如果有传newId，表示需要先跳转到分享页面（share.vue）,需要在分享表中记录多一条记录
				if(this.isNotNull(this.shareData.newId)){
					this.recordShareInfo(this.shareData.docUnid,this.shareData.newId)
				}
				// 生成uuid--分享id
				let NewId = this.guid();// 生成uuid--分享id
				const userInfo = uni.getStorageSync('userInfo');
				if(this.isNotNull(userInfo.userId)){
					let result = await this.$u.api.unifyRequest({
						appid: 'AppletInterface',
						wf_num: 'R_AppletInterface_B019',
						WF_OrUnid: this.shareData.docUnid,
						NewId: NewId,
						inviter: userInfo.userId,
						inviterName: userInfo.nickName,
						type: '0',
						// shareType: res.type,
						shareAddress: this.shareData.url,
						// shareSource: '/pages_tabbar_contact/common_contact/guide?type='+res.type+'&uid='+NewId,
						shareSource: '/pages_tabbar_contact/common_contact/guide?uid='+NewId,
						loading: true, //默认发起请求会有一个全局的Loading，设置false可去掉
					});
					if(result.code == 200){//执行回调函数
						let obj = {
							WF_OrUnid: this.shareData.docUnid,
							NewId: NewId,
							inviter: userInfo.userId,
							inviterName: userInfo.nickName,
							type: '0',
							// shareType: res.type,
							shareAddress: this.shareData.url,
							// shareSource: '/pages_tabbar_contact/common_contact/guide?type='+res.type+'&uid='+NewId,
							shareSource: '/pages_tabbar_contact/common_contact/guide?uid='+NewId,
						}
						this._runShareCallBack(obj)
					}
					return {
						title: this.shareData.shareTitle?this.shareData.shareTitle:'分享邀请',
						// path: '/pages_tabbar_contact/common_contact/guide?type='+res.type+'&uid='+NewId,
						path: '/pages_tabbar_contact/common_contact/guide?uid='+NewId,
						imageUrl: this.shareData.imageUrl
					}
					
				} else {
				  this.$u.toast("请先登录！")
				}
			// }
		},
		
		/**
		 * 调用动作后的前端回调函数
		 * @param {*} obj
		 */
		_runShareCallBack(obj) {
			console.log("************************_runShareCallBack,  try tun _runShareCallBack...************************", obj);
			try {
				this.shareSuccessCallBack(obj);
			} catch (error) {
			}
		},
		
		/**
		* 生成uuid
		*/
		guid() {
		  return (this.S4() + this.S4() + this.S4() + this.S4() + this.S4() + this.S4() + this.S4() + this.S4());
		},
		
		S4() {
		  return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
		}
		
	}

	
}