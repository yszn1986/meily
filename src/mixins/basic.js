/*
 * @Author: likang xie
 * @Date: 2020-10-22 10:18:17
 * @Purpose: 通用混合器
 */

import {mapMutations, mapState} from "vuex";
import globalData from "@/globalData";
import { hex_md5 } from "./md5";

export default {
  data() {
    return {
      $query: {}, // 页面参数
      $globalData: {}, // 全局变量
    };
  },

  mounted() {
    this.$globalData = globalData;
  },

  onLoad(options) {
    this.$query = options;
    this.$globalData = globalData;

    // #ifdef MP-WEIXIN
    // 激活分享朋友圈按钮
    wx.showShareMenu({
      withShareTicket: true,
      menus: ["shareAppMessage", "shareTimeline"],
    });
    // #endif
  },

  /**
   * 分享到好友
   */
  onShareAppMessage() {
    return {
      title: "",
      path: "",
      imageUrl: "",
    };
  },

  /**
   * 分享到朋友圈
   */
  onShareTimeline() {
    return {
      title: "",
      path: "",
      imageUrl: "",
    };
  },

  methods: {
    /**
     * 打开标签选择界面
     */
    toTagSettingPage(prevPageIsSelectList = [], mode = 1) {
      uni.setStorageSync("prevPageIsSelectList", prevPageIsSelectList);
      this.$u.route(`/pages_tabbar_contact/personal_contact/personal/setting/tag?mode=${mode}`);
    },

    /**
     * 打开附件上传界面
     */
    toAttachmentPage(prevPageIsUploadAttachmentList = [], mode = 1) {
      uni.setStorageSync(
        "prevPageIsUploadAttachmentList",
        prevPageIsUploadAttachmentList
      );

      this.$u.route(`/pages/index/attachment?mode=${mode}`);
    },

    /**
     * 返回上一步
     */
    goBack() {
		// this.$u.route({
		// 	type: "navigateBack",
		// });
		uni.navigateBack();
	},
		
	// =============================================这里是与支付相关的函数=====================================================
	/**
	 * 获取当前时间并格式化为yyyy-MM-dd HH:mm:ss函数
	 * @return 得到类似 2021-06-23 12:07:33 的值
	 */
	_getTimes() {
      const thisDay= new Date();
      const year = thisDay.getFullYear();       //年
      const month = thisDay.getMonth() + 1;     //月
      const day = thisDay.getDate();            //日
      const hh = thisDay.getHours();            //时
      const mm = thisDay.getMinutes();          //分
      const ss = thisDay.getSeconds();          //秒
      let timer = year + "-";
      if(month < 10)  
      timer += "0";
      timer += month + "-";
      if(day < 10)
      timer += "0";
      timer += day + " ";
      if(hh < 10)
      timer += "0";
        timer += hh + ":";
      if (mm < 10) 
      timer += '0'; 
      timer += mm + ":"; 
      if (ss < 10) 
      timer += '0'; 
      timer += ss; 
      return (timer);
    },
	/**
	 * 调用JDK版本的微信支付接口进入鉴权获取 token 和 authstr
	 *  obj 中可以包含的参数有以下
	 * 		obj.userid, 平台用户身份，用户id：0507062149075060
	 * 		obj.openid, 小程序身份id：oisk64zYcJz8Atp1P2VZefgHvM_4
	 * 		obj.tradeNo, 商户订单号：160675802522101302264851810598
	 * 		obj.tradeDesc, 订单详情或描述：YL2-测试商品-支付团购费用
	 * 		obj.payPrice, 支付金额，字符串类型，单位分：1
	 * 		obj.refundNo, 退款订单号，只在退款单时有用：160675802522101302264851810595
	 * 		obj.refundPrice, 退款金额，字符串类型，单位分：1
	 * 		obj.productID, 订单对应的商品id：1580842896171601920
	 * 		obj.productUrl, 订单对应的小程序打开地址：pages_tabbar_contact/index_contact/pay
	 * 		obj.productUrl, 订单对应的图片：https://www-1308272144.cos.ap-guangzhou.myqcloud.com/static/image/perpartner.png
	 */
	async geAuthInfo(obj){
		let authinfo = {};
		authinfo.code = 201;
		let res = await this.$u.api.unifyMiniRest({
			systemid: "wxpay", //系统名称
			url: "/api/getoken", //接口请求地址
			// 这里是需要鉴权的参数列表
			userid: obj.userid,
			openid: obj.openid,
			tradeNo: obj.tradeNo,
			tradeDesc: obj.tradeDesc,
			payPrice: obj.payPrice,
			refundNo: obj.refundNo,
			refundPrice: obj.refundPrice,
			productID: obj.productID,
			productUrl: obj.productUrl,
			imageUrl: obj.imageUrl,
			goodsInfo: JSON.stringify(obj.goodsInfo),
			mchInfo: JSON.stringify(obj.mchInfo),
			remark: obj.remark,
			orderExtJson: obj.orderExtJson,
			loading: true
		});
		if(res.code == 200){
			authinfo.code = 200;
			authinfo.token = res.data.token;
			let authsalt = globalData.authsalt;
			// 2 8 10 5 位，前两位 char 转 ASCII 码相加
			let tmpToken = authsalt.charCodeAt(2) + authsalt.charCodeAt(8) + res.data.token + authsalt.charAt(10) + authsalt.charAt(5);
			authinfo.authstr = hex_md5(tmpToken);
		}
		return authinfo;
	},
	
	/**
	 * 调用JDK版本的微信支付接口进行请求操作
	 * @param {Object} url 请求接口地址
	 * @param {Object} obj 请求参数，不包含鉴权参数 token 和 authstr
	 * @param {Object} authinfo 请求鉴权接口获得的参数，包含 token 和 authstr
	 */
	async postWxpayServer(url, obj, authinfo){
		let res = await this.$u.api.unifyMiniRest({
			systemid: "wxpay", //系统名称
			url: url, //接口请求地址
			// 这里是需要鉴权的参数列表
			userid: obj.userid,
			openid: obj.openid,
			tradeNo: obj.tradeNo,
			tradeDesc: obj.tradeDesc,
			payPrice: obj.payPrice,
			refundNo: obj.refundNo,
			refundPrice: obj.refundPrice,
			productID: obj.productID,
			productUrl: obj.productUrl,
			imageUrl: obj.imageUrl,
			goodsInfo: JSON.stringify(obj.goodsInfo),
			mchInfo: JSON.stringify(obj.mchInfo),
			remark: obj.remark,
			token: authinfo.token,
			authstr: authinfo.authstr,
			orderExtJson: obj.orderExtJson,
			loading: true
		});
		return res;
	},
	// =============================================这里是与支付相关的函数=====================================================

	  /**
	   * 获取格式化后的昵称
	   * @param userId    用户id
	   * @param applyUser 需求发布者id
	   * @param docUnId   当前文档id
	   * @param type      getNick(获取格式化昵称)/getNickAndPic(获取格式化昵称和头像链接)
	   * @returns {Promise<void>}
	   */
	  async getFormatNick(userId,applyUser,docUnId,type){
		  return await this.$u.api.unifyRequest({
			  appid: 'AppletsTool',
			  wf_num: 'R_AppletsTool_B003',
			  userId: userId,
			  applyUser: applyUser,
			  docUnId: docUnId,
			  type: type
		  });
	  },
    //解析标签
    ParseLabel(labels) {
      let label_value = ""
      let labelJson = JSON.parse(JSON.stringify(labels))
      for(let i=0; i<labelJson.length; i++){
        let temp = labelJson[i].value
        if(i != 0){
          if(temp!=null && temp!=''){
            label_value += "," + temp
          }
          //label_value += "," + labelJson[i].value
        }else{
          if(temp!=null && temp!=''){
            label_value = temp
          }
          //label_value = labelJson[i].value
        }
      }
      return label_value
    },
    //渲染标签
    RestructuringLabel(labels){
      if(labels != ''){
        let new_labels = "["
        let labelsArr = labels.split(",")
        for(let i=0; i<labelsArr.length; i++){
          if(i != 0){
            let json_str = ',{\"isSelect\":\"true\",\"label\":\"'+labelsArr[i]+'\",\"value\":\"'+labelsArr[i]+'\"}'
            new_labels += json_str
          }else{
            let json_str = '{\"isSelect\":\"true\",\"label\":\"'+labelsArr[i]+'\",\"value\":\"'+labelsArr[i]+'\"}'
            new_labels += json_str
          }
        }
        new_labels += "]"
        return JSON.parse(new_labels)
      }
      return ""
    },
	
	//渲染标签(渲染为未选中)
	RestructuringLabelUnchecked(labels){
	  if(labels != ''){
	    let new_labels = "["
	    let labelsArr = labels.split(",")
	    for(let i=0; i<labelsArr.length; i++){
	      if(i != 0){
	        let json_str = ',{\"isSelect\":\"\",\"label\":\"'+labelsArr[i]+'\",\"value\":\"'+labelsArr[i]+'\"}'
	        new_labels += json_str
	      }else{
	        let json_str = '{\"isSelect\":\"\",\"label\":\"'+labelsArr[i]+'\",\"value\":\"'+labelsArr[i]+'\"}'
	        new_labels += json_str
	      }
	    }
	    new_labels += "]"
	    return JSON.parse(new_labels)
	  }
	  return ""
	},

    //转义HTML代码
    escape2Html (str) {
      let arrEntities = { 'lt': '<', 'gt': '>', 'nbsp': ' ', 'amp': '&', 'quot': '"' };
	  if(this.isNotNull(str)){
		  return str.replace(/&(lt|gt|nbsp|amp|quot);/ig, function (all, t) { return arrEntities[t]; });
	  }
    },
	
	//转义HTML代码
	escape2Html2 (str){
		return str.replace(/(\r\n|\n|\r)/gm, '<br />');
	},

    /**
     * 是否包含特殊符号
     * @param str 要检测的字符串
     * @returns {boolean}
     */
    hasSpecial(str){
      let pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>《》/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？ ]");
      if (pattern.test(str)){
        return true;
      }
      return false;
    },
	
	/**
	 * 判断值是否不为空
	 * @param {Object} val
	 */
	isNotNull(val){
		if(val!=='undefined' && val!=="" && val!=null){
			return true
		}
		return false
	},
	
	/**
	 * 根据状态获取标签属性
	 */
	getAttrByStatus(status){
		switch(status) {
			case '审核中':
				return ['warning','dark','mini'];
				break;
			case '不通过':
				return ['info','light','mini'];
				break;
			case '平台审核':
				return ['warning','dark','mini'];
				break;
			case '待处理':
				return ['warning','dark','mini','#cf00f4'];
				break;
			case '匹配中':
				return ['primary','dark','mini'];
				break;
			case '待复选':
				return ['primary','dark','mini','#0600f4'];
				break;
			case '待参与复选':
				return ['primary','dark','mini','#0600f4'];
				break;
			case '已处理':
				return ['primary','dark','mini','#0600f4'];
				break;
			case '已确认，支付项目款':
				return ['error','dark','mini','#e33a37'];
				break;
			case '已支付，填写指标':
				return ['error','dark','mini','#e33a37'];
				break;
			case '等待确认':
				return ['error','dark','mini','#e33a37'];
				break;
			case '工作进行中':
				return ['error','dark','mini','#e33a37'];
				break;
			case '支付项目款':
				return ['error','dark','mini','#e33a37'];
				break;
			case '已支付，等待验收':
				return ['error','dark','mini','#e33a37'];
				break;
			case '已验收，等待放款':
				return ['error','dark','mini','#e33a37'];
				break;
			case '待评价':
				return ['error','dark','mini','#e33a37'];
				break;
			case '交付已完成':
				return ['error','dark','mini','#e33a37'];
				break;
			case '匹配失败':
				return ['info','light','mini'];
				break;
			case '已过期':
				return ['info','light','mini'];
				break;
			case '待初选':
				return ['warning','dark','mini'];
				break;
			case '待确认，已支付保证金':
				return ['warning','dark','mini','#cf00f4'];
				break;
			case '支付保证金':
				return ['warning','dark','mini','#cf00f4'];
				break;
			case '已完成':
				return ['success','dark','mini','#0ec800'];
				break;
			case '火热报名':
				return ['error','dark','mini'];
				break;
			case '初选中':
				return ['warning','dark','mini'];
				break;
			case '不通过':
				return ['info','light','mini'];
				break;
			case '已通过':
				return ['error','dark','mini','#e33a37'];
				break;
			case '已结束':
				return ['info','light','mini'];
				break;
			case '复选中':
				return ['primary','dark','mini','#0600f4'];
				break;
			case '参与复选':
				return ['primary','dark','mini','#0600f4'];
				break;
			case '已中标':
				return ['warning','dark','mini','#cf00f4'];
				break;
			case '等待启动':
				return ['error','dark','mini','#e33a37'];
				break;
			case '确认指标':
				return ['warning','dark','mini','#cf00f4'];
				break;
			case '工作进行中':
				return ['warning','dark','mini','#cf00f4'];
				break;
			case '交付成果':
				return ['warning','dark','mini','#cf00f4'];
				break;
			case '等待验收':
				return ['warning','dark','mini','#cf00f4'];
				break;
			case '交付已完成':
				return ['warning','dark','mini','#cf00f4'];
				break;
			case '设置指标':
				return ['warning','dark','mini','#cf00f4'];
				break;
			default:
				return ['error','dark','mini'];
				break;
		} 
	},
	
	/**
	 * 保存手机号
	 */
	savePhoneNumber(cloudID) {
		const userInfo = uni.getStorageSync('userInfo')
		wx.cloud.callFunction({
			name: 'getMobile',
			data: {
				weRunData: wx.cloud.CloudID(cloudID),
			}
		}).then(res => {
			let phoneNumber = res.result
			let data = this.$u.api.unifyRequest({
				userId: userInfo.userId,
				appid: 'AppletInterface',
				wf_num: 'R_AppletInterface_B014',
				action: 'save',
				phoneNumber: phoneNumber
			});
			userInfo.phoneNumber = phoneNumber
			this.SET_USER_INFO(userInfo)
			return true
		}).catch(err => {
			console.error(err);
			return false
		 });
	},
	
	/**
	* 附件预览
	*/
	async preview(file_path,file_type) {
	  let url = globalData.remote + file_path;
	    if(file_type=='jpg' || file_type=='png'){
	      wx.previewImage({
	        urls: [url]
	      })
	    }else{
	      //下载附件
	      wx.downloadFile({
	              url: url,
	              success: function (res) {
	                var filePath = res.tempFilePath
	                console.log(filePath)
	              
	                //预览附件
	                wx.openDocument({
	                  filePath: filePath,
	                  fileType: file_type,
	                  success: function (res) {
	                    console.log("打开文档成功")
	                    console.log(res);
	                  },
	                  fail: function (res) {
	                    console.log("fail");
	                    console.log(res)
	                  },
	                  complete: function (res) {
	                    console.log("complete");
	                    console.log(res)
	                  }
	                })
	              },
	              fail: function (res) {
	                console.log('fail')
	                console.log(res)
	              },
	              complete: function (res) {
	                console.log('complete')
	                console.log(res)
	              }
	      })
	    }
	},
	 async addVipTask(taskId,userId){
		 const userInfo = uni.getStorageSync('userInfo')
		 if(!userId){
			 userId = userInfo.userId
		 }
		return await this.$u.api.unifyRequest({
			 appid: 'ACTIVITY',
			 wf_num: 'R_ACTIVITY_BW005',
			 userId: userId,
			 taskId: taskId,
			 loading: false // 默认发起请求会有一个全局的Loading，设置false可去掉
		 });
	},
	/**
	 * 提示框
	 * statu: success、error、loading、none
	 */
	showMSG(statu,msg){
		wx.showToast({
			icon: statu,
			title: msg,
			duration: 2000
		});
	},
	
	/**
	 * 获取当前页面地址
	 */
	getCurrentPageUrl(that){
		return that.$mp.page.route;
	},
	
	/**
	 * 记录分享信息
	 */
	async recordShareInfo(WF_OrUnid,NewId){
		  const userInfo = uni.getStorageSync('userInfo');
		  let result = this.$u.api.unifyRequest({
				appid: 'AppletInterface',
				wf_num: 'R_AppletInterface_B019',
				WF_OrUnid: WF_OrUnid,
				NewId: NewId,
				inviter: userInfo.userId,
				inviterName: userInfo.nickName,
				type: '0',
				shareType: '1',
				loading: true, //默认发起请求会有一个全局的Loading，设置false可去掉
		  });
		  if(result.code == 200){
			  return true
		  }else{
			  return false
		  }
	},
	
	/**
	 * 图片/视频预览
	 * @param {Object} url 可直接访问地址
	 * @param {Object} fileSuffix 如：jpg、doc、xlsx等
	 */
	previewPictrueOrVideo(url, fileSuffix) {
		wx.previewMedia({
			sources: [
				{
					url: url, // 预览地址，可图片可视频
					type: fileSuffix=="mp4"?"video":"image" // image：图片，video: 视频
				}
			]
		});
	},
	
	/**
	 * 缓存中用户信息判断：如果没有获取到userInfo和userId都会跳转到登录页进行登录，登录完成返回到调用的页面
	 */
	CheckUserInfoCache(){
		const userInfo = uni.getStorageSync('userInfo')
		if(this.isNotNull(userInfo) && Object.prototype.toString.call(userInfo) === '[object Object]'){
			if(this.isNotNull(userInfo.userId) && typeof(userInfo.userId)=='string'){
				return true
			}
		}
		//未登录跳转到登陆页
		this.$u.route('/pages_tabbar_contact/common_contact/simpleWechatLogin')
	},
	
	/**
	* 生成uuid
	*/
	guid() {
		let uuid = ''
		for (let i = 0; i < 8; i++) {
			if(i == 0){
				uuid = (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1)
			}else{
				uuid += (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1)
			}
		}
		return uuid
	},
	
	/** 
	 * 积分系统token加盐值并且md5加密
	 */
	sault_md5_token(token){
		let authsalt = globalData.authsalt
		let tmpToken = authsalt.charCodeAt(2) + authsalt.charCodeAt(8) + token + authsalt.charAt(10) + authsalt.charAt(5)
		let authStr = hex_md5(tmpToken)
		return authStr
	},
	
	/**
	 * 广告图跳转方法
	 */
	routeAD(link,source) {
		if(this.isNotNull(link) && this.isNotNull(source)) {
			if(source == 'applet'){
				if(link.indexOf('pages/index/') != -1){
					uni.reLaunch({url:link})
				}else{
					this.$u.route(link)
				}
			}else {
				this.$u.route('pages_tabbar_contact/common_contact/article?url='+encodeURIComponent(link))
			}
		}else {
			this.showMSG('none','链接地址或广告来源数据丢失')
		}
	},
	
	/**
	 * 长按复制
	 */
	longpressCopy(text,msg) {
		uni.setClipboardData({
			data: text,
			success: function () {
				uni.showToast({
					title:msg,
					icon:'none'
				})
			}
		});
	},
	
	/**
	 * uview自定义导航栏返回上一页
	 */
	comeback(){
		let pages = getCurrentPages()
		if(pages[pages.length - 2]){
			//如果有上一页，就返回上一页
			this.goBack()
		}else{
			//如果没有上一页，则返回首页
			uni.reLaunch({url:'/pages/index/index'});
		}
	},
	
	/**
	 * dateStr为相加前的时间， days 为 需要相加的天数
	 * @param {Object} date
	 * @param {Object} days
	 */
	addDateTime(dateStr, days) {
		let date = new Date(dateStr.replace(/-/g,"/"));
		date.setDate(date.getDate() + days);
		let year = date.getFullYear();
		let month = date.getMonth() + 1;
		let day = date.getDate();
		let hours = date.getHours();
		let minutes = date.getMinutes();
		let seconds = date.getSeconds();
		month = month < 10 ? "0" + month : month;
		day = day < 10 ? "0" + day : day;
		hours = hours < 10 ? "0" + hours : hours;
		minutes = minutes < 10 ? "0" + minutes : minutes;
		seconds = seconds < 10 ? "0" + seconds : seconds;
		let time = year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
		return time;
	},
	
	/**
	 * date为相加前的时间， days 为 需要相加的天数
	 * @param {Object} date
	 * @param {Object} days
	 */
	addDate(date, days) {
		var date = new Date(date);
		date.setDate(date.getDate() + days);
		var year = date.getFullYear();
		var month = date.getMonth() + 1;
		var day = date.getDate();
		var mm = "'" + month + "'";
		var dd = "'" + day + "'";
		if (mm.length == 3) {
			month = "0" + month;
		}
		if (dd.length == 3) {
			day = "0" + day;
		}
		var time = year + "-" + month + "-" + day;
		return time;
	},
	
	/**
	 * 获取倒计时(时：分：秒)
	 * dateStr为相加前的时间， days 为 需要相加的天数
	 * @param {Object} dateStr
	 * @param {Object} days
	 */
	getCountdown(dateStr, days) {
		let date = new Date(dateStr.replace(/-/g,"/"));
		let hours = date.getHours() + days * 24;
		let minutes = date.getMinutes();
		let seconds = date.getSeconds();
		hours = hours < 10 ? "0" + hours : hours;
		minutes = minutes < 10 ? "0" + minutes : minutes;
		seconds = seconds < 10 ? "0" + seconds : seconds;
		let time = hours + ":" + minutes + ":" + seconds;
		return time;
	},
	
	/**
	 * 获取今天距离结束日期的剩余时间（时：分：秒）
	 */
	getTimeRemaining(endDateStr) {
		let today = this._getTimes(); // 获取当前日期
		let startTime = new Date(today);
		let endTime = new Date(endDateStr.replace(/-/g,"/")); //timestamp * 1000
		let t = endTime.getTime() - startTime.getTime();
		if (t > 0) {
			let day = Math.floor(t / 86400000);
			let hour = Math.floor((t / 3600000) % 24);
			let min = Math.floor((t / 60000) % 60);
			let sec = Math.floor((t / 1000) % 60);
			if (day > 0) {
				hour = day * 24;
			}
			hour = hour < 10 ? "0" + hour : hour;
			min = min < 10 ? "0" + min : min;
			sec = sec < 10 ? "0" + sec : sec;
			return `${hour}:${min}:${sec}`;
		}
	},
	
	/**
	 * 计算日期差：不足一天按一天算，即向上取值
	 * startTime 今天
	 * endTime   上架时间
	 * 返回天数差
	 */
	comparisonDate(startTime, endTime) {
		// 日期格式化
		let start_date = new Date(startTime.replace(/-/g, "/"));
		let end_date = new Date(endTime.replace(/-/g, "/"));
		// 转成毫秒数，两个日期相减
		let ms = start_date.getTime() - end_date.getTime();
		// 转换成天数(向上取整，不足一天按一天算)
		let day = Math.ceil(ms / (1000 * 60 * 60 * 24));
		return day;
	},
	
	/**
	 * 倒计时
	 */
	countdowm(timestamp, defaulttext) {
		let self = this
		let now = new Date()//获取当前时间戳
		let future = new Date(timestamp.replace(/-/g,"/"))//距离的时间
		let index = (future-now)/1000/(60*60*24)
		let day = parseInt(index)//天
		if(day > 3 || day == 3){
			self.endTimeContent = day + '天后结束';
		}else{
			let timer = setInterval(function() {
				let nowTime = new Date();
				let endTime = new Date(timestamp.replace(/-/g,"/")); //timestamp * 1000
				let t = endTime.getTime() - nowTime.getTime();
				if (t > 0) {
					let day = Math.floor(t / 86400000);
					let hour = Math.floor((t / 3600000) % 24);
					let min = Math.floor((t / 60000) % 60);
					let sec = Math.floor((t / 1000) % 60);
					if (day > 0) {
						hour = day * 24 + hour;
					}
					hour = hour < 10 ? "0" + hour : hour;
					min = min < 10 ? "0" + min : min;
					sec = sec < 10 ? "0" + sec : sec;
					self.endTimeContent = `${hour}:${min}:${sec}`;
				} else {
					clearInterval(timer);
					self.endTimeContent = defaulttext;
				}
			}, 1000);
		}
	},
	
	/**
	 * 校验收货人姓名
	 * 至少包含一个A—>Z(不区分大小写)  或 至少1个中文
	 * @param {Object} val
	 */
	verificationName(name){
		// let reg = new RegExp("/[A-Za-z\u4E00-\u9FA5a]+$/");
		let reg = /[A-Za-z\u4E00-\u9FA5a]+$/
		if(reg.test(name.trim())){
		    return true;
	    } else {
			return false;
		}
	},
	
	/**
	 * 校验手机号码是否合法
	 * @param {Object} phoneNum
	 */
	verificationPhone(phoneNum){
		let reg = /^1[3456789]\d{9}$/
		// ^1  以1开头
		// [3456789] 第2位，使用原子表里的任意一个原子都可以
		// \d{9}$  第三位  朝后可以是任意数字  并且最后结尾必须是数字
		if(reg.test(phoneNum.trim())){
			return true;
		} else {
			return false;
		}
	},
	
	/**
	 * 公告通用组件提供给子页面的检查公告方法回调
	 */
	reloadNotice() {
		this.$refs.noticeModule.isShowNotice()
	},

	/**
	 * 将图片用逗号分隔成数组并返回第一张图片
	 */
	getFirstImg(imgs){
		if(this.isNotNull(imgs)){
			let imgsArr = imgs.split(",")
			return imgsArr[0]
		}
		return ''
	},
	
    ...mapMutations(["SET_USER_INFO"]),
  },

  computed: {
    ...mapState(["userInfo"]),
  },
};
