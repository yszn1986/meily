/*
 * @Author: likang xie
 * @Date: 2020-09-29 11:48:23
 * @Purpose: 状态管理入口
 */

import Vue from "vue";
import Vuex from "vuex";
import globalData from "@/globalData";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    userInfo: uni.getStorageSync("userInfo") || {}, // 用户信息
  },

  mutations: {
    // 设置用户信息
    SET_USER_INFO(state, userInfo = {}) {
	  userInfo.version = globalData.version;
      state.userInfo = userInfo;
      uni.setStorageSync("userInfo", userInfo);
    },
  },
});
