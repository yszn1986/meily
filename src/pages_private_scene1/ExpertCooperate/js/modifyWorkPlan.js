export default {
  data() {
    return {
      form: {
        workContent: "",
        deliveryDate: "",
        bidPrice: "",
        nickName: "",
        paymentType: "",
      },
      deliveryDateShow: false,
      subDateShow: false,
      subDateIndex: 0,

      timeParams: {
        year: true,
        month: true,
        day: true,
        hour: false,
        minute: false,
        second: false,
      },
      planJsonData: [],
      itemStyle: {},
      labelStyle: {},
      taskNumber: "",
      userId: "",
    };
  },
  onShow() {},
  onLoad(options) {
    this.taskNumber = options.taskNumber;
    this.userId = options.userId;
    if (this.taskNumber) {
      this.getJobDetail().then(() => {
        this.planJsonData = JSON.parse(this.form.workContent);
        this.form.deliveryDate =
          this.planJsonData[this.planJsonData.length - 1].subDate;
        this.form.paymentType = options.paymentType;
      });
    }
    this.itemStyle = {
      "border-bottom": "1px solid rgb(230, 230, 230)",
      padding: "0 8rpx",
      "font-size": "28rpx",
      "font-weight": "bold",
    };
    this.labelStyle = {
      "font-weight": "normal",
    };
  },
  methods: {
    async getJobDetail() {
      let result = await this.$u.api.unifyMiniRest({
        systemid: "meily",
        url: "/expert/signup/detail",
        taskNum: this.taskNumber, // 附件唯一id
        userId: this.userId,
        loading: false, // 默认发起请求会有一个全局的Loading，设置false可去掉
      });
      if (result.code === "200" && result.data) {
        this.form = result.data;
      } else {
        this.$u.toast(result.msg);
      }
      this.$nextTick(() => {
        this.$refs.collapseView.init();
      });
    },
    autoHeight() {
      this.$nextTick(() => {
        this.$refs.collapseView.init();
      });
    },
    showSelect(index) {
        this.subDateShow = true;
        this.subDateIndex = index-1;
    },

    modifyPlanDate(event, index) {
      this.planJsonData[index].subDate = `${event.year}-${event.month}-${event.day}`;
      if (index == this.planJsonData.length - 1) {
        this.form.deliveryDate = this.planJsonData[index].subDate;
      }
      let flag = false;
      if (index >= 1) {
        let prvDate = new Date(this.planJsonData[index - 1].subDate);
        let curDate = new Date(this.planJsonData[index].subDate);
        flag = curDate.getTime() <= prvDate.getTime();
        if (!this.planJsonData[index - 1].subDate) {
          this.$u.toast("请先设置第" + index + "期交付日期!");
          this.planJsonData[index].subDate = "";
          return false;
        }
        if (index + 1 == this.planJsonData.length) {
          this.form.deliveryDate = this.planJsonData[index].subDate;
        }
        if (flag) {
          this.$u.toast(
            "第" + (index + 1) + "期交付日期必须大于第" + index + "期！"
          );
          this.planJsonData[index].subDate = "";
        }
      }
    },
    //计算每一期的佣金
    modifyPlanBrokerage() {
      if (this.form.bidPrice < this.form.budget) {
        this.form.bidPrice = "";
        this.$u.toast("不可低于项目预算：" + this.form.budget + "元");
        return false;
      }
      let array = this.form.paymentType.split(":");
      if (array.length > 1) {
        array.forEach((item, index) => {
          let sort = item.charAt(item.length - 1);
          sort = "0." + sort;
          this.planJsonData[index].brokerage = this.accMul(
            this.form.bidPrice,
            sort
          );
        });
      } else {
        this.planJsonData[0].brokerage = this.form.bidPrice;
      }
    },
    /**
     * 乘法计算
     * @param a
     * @param b
     * @returns {number}
     */
    accMul(a, b) {
      let getMul = (num) =>
        num.toString().indexOf(".") == -1
          ? 0
          : num.toString().split(".")[1].length;
      let mathpow = (a) => a * 10 ** getMul(a);
      return (mathpow(a) * mathpow(b)) / 10 ** (getMul(a) + getMul(b));
    },
    showDemand() {
      this.$u.route(
        "/pages_private_scene1/ExpertCooperate/jobInfo?taskNumber=" +
          this.taskNumber
      );
    },
    //请求提交流程接口
    async signupDemand() {
      this.planJsonData.forEach((item, index) => {
        if (!item.subDate) {
          this.$u.toast(
            "请设置完成第" + (index + 1) + "期交付日期再提交计划！"
          );
          return false;
        }
        if (this.planJsonData.length - 1 == index) {
          let prvDate = new Date(this.form.deliveryDate);
          let curDate = new Date(this.planJsonData[index].subDate);
          if (curDate.getTime() < prvDate.getTime()) {
            this.$u.toast("最后一期交付日期不能小于预计完成日期！");
            return false;
          }
        }
      });
      const userInfo = uni.getStorageSync("userInfo");
      //提交流程
      let result = await this.$u.api.unifyMiniRest({
        systemid: "meily",
        url: "/expert/job/reselect/user",
        userId: userInfo.userId,
        users: this.userId,
        taskNum: this.taskNumber,
        workContent: this.planJsonData,
        finalPrice: this.form.bidPrice,
        deliveryDate: this.form.deliveryDate,
      });
      if (result.code == "200") {
        uni.redirectTo({
          url:
            "/pages_private_scene1/ExpertCooperate/common/result?mode=51&taskNumber=" +
            this.taskNumber,
        });
      } else {
        this.$u.toast(result.msg);
      }
    },
  },
};
