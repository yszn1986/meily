export default {
  data() {
    return {
      form: {
        subject: "",
        taskNum: "",
        budget: "",
        paymentType: "",
        deadline: "",
        details: "",
        createUser: "",
      },
      nickName: "",
      taskNumber: "",
    };
  },

  onLoad(options) {
    this.taskNumber = options.taskNumber;
    if (this.taskNumber) {
      this.getJobDetail();
    }
  },
  methods: {
    async getJobDetail() {
      let result = await this.$u.api.unifyMiniRestGet({
        systemid: "meily",
        url: "/expert/job/" + this.taskNumber,
        loading: true, // 默认发起请求会有一个全局的Loading，设置false可去掉
      });
      if (result.code === "200" && result.data) {
        this.form = result.data;
        this.getNickName(this.form.createUser);
      } else {
        this.$u.toast(result.msg);
      }
    },
    async getNickName(userId) {
      let result = await this.$u.api.unifyMiniRestGet({
        systemid: "sso",
        url: "/user/cache/" + userId,
        loading: true, // 默认发起请求会有一个全局的Loading，设置false可去掉
      });
      if (result.code == "200" && result.data) {
        this.nickName = result.data.nickName;
      }
    },
    showDemand() {
      this.$u.route(
        "/pages_private_scene1/ExpertCooperate/jobInfo?taskNumber=" +
          this.taskNumber
      );
    },
    //请求提交流程接口
    signupDemand() {
      this.$u.route(
        "/pages_private_scene1/ExpertCooperate/partnerApplyAgain2?taskNumber=" +
          this.taskNumber
      );
    },
  },
};
