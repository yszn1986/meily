export default {
  data() {
    return {
      form: {
        subject: "",
        taskNum: "",
        budget: "",
        paymentType: "",
        deadline: "",
        details: "",
        bio: "",
      },
      isShow: "1",
      caseJsonData: [],
      ResumeFiles: [],
      taskNumber: "",
    };
  },

  onLoad(options) {
    this.caseJsonData.forEach((item, index) => {
      uni.$on("saveTagList" + index, (tagList = []) => {
        item.skills = tagList;
      });
    });
    this.isShow = this.$query.isShow;
    if (this.isShow == "1") {
      //如果需要显示报名按钮，需要验证是否登录
      this.checkLogin();
      return;
    }
    this.taskNumber = options.taskNumber;
    if (this.taskNumber) {
      this.getJobDetail();
    }
  },
  onUnload() {
    this.caseJsonData.forEach((item, index) => {
      uni.$off("saveTagList" + index);
    });
  },
  methods: {
    async getJobDetail() {
      let result = await this.$u.api.unifyMiniRestGet({
        systemid: "meily",
        url: "/expert/job/" + this.taskNumber,
        loading: true, // 默认发起请求会有一个全局的Loading，设置false可去掉
      });
      if (result.code === "200" && result.data) {
        this.form = result.data;
      } else {
        this.$u.toast(result.msg);
      }
    },
    //请求提交流程接口
    async signupDemand() {
      const userInfo = uni.getStorageSync("userInfo");
      let result = await this.$u.api.unifyMiniRest({
        systemid: "meily",
        url: "/expert/job/signup/partner",
        userId: userInfo.userId,
        nickName: userInfo.nickName,
        taskNum: this.taskNumber,
        projectCase: this.caseJsonData,
        bio: this.form.bio,
        jobId: this.form.jobId,
      });
      if (result.code === "200") {
          uni.redirectTo({
            url:
              "/pages_private_scene1/ExpertCooperate/common/result?mode=2&taskNumber=" +
              this.taskNumber,
          });
      } else {
        this.$u.toast(result.msg);
      }
    },
    //添加案例
    async addCase(action) {
      if (action == "1") {
        this.isShow = "2";
      } else {
        let str = '{"caseName":"","caseDetails":"","skills":""}';
        this.caseJsonData.push(JSON.parse(str));
        this.caseJsonData.forEach((item, index) => {
          uni.$on("saveTagList" + index, (tagList = []) => {
            item.skills = tagList;
          });
        });
      }
    },
    checkLength(value) {
      if (value && value.length < 4) {
        this.$u.toast("不能少于四个字！");
        return false;
      }
    },
    //删除案例
    removeCase() {
      const length = this.caseJsonData.length;
      if (length > 0) {
        this.caseJsonData.splice(length - 1, 1);
      }
      this.caseJsonData.forEach((item, index) => {
        uni.$on("saveTagList" + index, (tagList = []) => {
          item.skills = tagList;
        });
      });
    },
    //打开标签页
    toTagSettingPage(prevPageIsSelectList = [], mode = 1, type) {
      uni.setStorageSync("create", prevPageIsSelectList);
      this.$u.route(
        `/pages_private_scene1/ExpertCooperate/common/tag?mode=${mode}&type=${type}`
      );
    },
    //去除标签
    onCloseTag(i, index) {
      this.caseJsonData.filter((item, index2) => {
        if (index2 === index) {
          item.skills.filter((item2, index3) => {
            if (index3 === i) {
              item.skills.splice(index3, 1);
            }
          });
        }
      });
    },
    guid() {
      return (
        this.S4() +
        this.S4() +
        this.S4() +
        this.S4() +
        this.S4() +
        this.S4() +
        this.S4() +
        this.S4()
      );
    },

    S4() {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    },
  },
};
