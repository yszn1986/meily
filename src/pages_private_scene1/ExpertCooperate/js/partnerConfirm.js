export default {
  data() {
    return {
      form: {
        fixWorkContent: "",
        deliveryDate: "",
        finalPrice: "",
      },
      subject: "",
      taskNumber: "",
      planJsonData: [],
      itemStyle: {},
      labelStyle: {},
    };
  },
  onShow() {},
  onLoad(options) {
    this.taskNumber = options.taskNumber;
    if (this.taskNumber) {
      this.getBidDetail().then(() => {
        this.planJsonData = JSON.parse(this.form.fixWorkContent);
      });
      this.getJobSubject();
    }
    this.itemStyle = {
      "border-bottom": "1px solid rgb(230, 230, 230)",
      padding: "0 8rpx",
      "font-size": "28rpx",
      "font-weight": "bold",
    };
    this.labelStyle = {
      "font-weight": "normal",
    };
  },
  methods: {
    showDemand() {
      this.$u.route(
        "/pages_private_scene1/ExpertCooperate/jobInfo?taskNumber=" +
          this.taskNumber
      );
    },
    async getJobSubject() {
      let result = await this.$u.api.unifyMiniRestGet({
        systemid: "meily",
        url: "/expert/job/" + this.taskNumber,
      });
      if (result.code === "200" && result.data) {
        this.subject = result.data.subject;
      }
    },
    async getBidDetail() {
      const userInfo = uni.getStorageSync("userInfo");
      let result = await this.$u.api.unifyMiniRest({
        systemid: "meily",
        url: "/expert/signup/detail",
        taskNum: this.taskNumber, // 附件唯一id
        userId: userInfo.userId,
        loading: false, // 默认发起请求会有一个全局的Loading，设置false可去掉
      });
      if (result.code === "200" && result.data) {
        this.form = result.data;
      } else {
        this.$u.toast(result.msg);
      }
      this.$nextTick(() => {
        this.$refs.collapseView.init();
      });
    },
    autoHeight() {
      this.$nextTick(() => {
        this.$refs.collapseView.init();
      });
    },
    async submit() {
      const userInfo = uni.getStorageSync("userInfo");
      let result = await this.$u.api.unifyMiniRest({
        systemid: "meily",
        url: "/expert/job/confirm/partner",
        userId: userInfo.userId,
        taskNum: this.taskNumber,
        nickName: userInfo.nickName,
        workContent: this.planJsonData,
        loading: false, // 默认发起请求会有一个全局的Loading，设置false可去掉
      });
      if (result.code == "200") {
        uni.redirectTo({
          url:
            "/pages_private_scene1/ExpertCooperate/common/result?mode=6&taskNumber=" +
            this.taskNumber +
            "&Partnerid=" +
            uni.getStorageSync("userInfo").userId,
        });
      } else {
        this.$u.toast(result.msg);
      }
    },
  },
};
