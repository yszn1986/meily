export default {
  data() {
    return {
      form: {
        subject: "",
        taskNum: "",
        budget: "",
        paymentType: "",
        deadline: "",
        details: "",
        remark: "",
      },
      taskNumber: "",
      userList: "",
      signUpUserList: [],
      passApply: false,
      refuseApply: false,
      showBtn: false,
      btnStyle: "margin-top:10rpx;bottom: 100rpx;width: 90%;",
    };
  },
  onLoad(options) {
    this.taskNumber = options.taskNumber;
    if (this.taskNumber) {
      this.getJobDetail();
      this.getSignUpUserList();
    }
  },

  methods: {
    passFirstSelect() {
      if (this.userList.length === 0) {
        this.$u.toast("请至少选择一个拍档通过！");
      } else {
        this.passApply = true;
      }
    },
    showTips(item) {
      this.$u.route(
        "/pages_private_scene1/ExpertCooperate/common/partnerInfo?userId=" +
          item.userId +
          "&taskNumber=" +
          this.taskNumber
      );
    },
    checkboxChange(e) {
      this.userSet = this.userSet || new Set();
      if (e.value) {
        this.userSet.add(e.name);
      } else {
        this.userSet.delete(e.name);
      }
      this.userList = Array.from(this.userSet).join(",");
    },
    //获取报名的最佳拍档
    async getSignUpUserList() {
      let result = await this.$u.api.unifyMiniRest({
        systemid: "meily",
        url: "/expert/signup/list",
        userId: uni.getStorageSync("userInfo").userId,
        taskNum: this.taskNumber, // 附件唯一id
        status: "初选中",
        loading: false, // 默认发起请求会有一个全局的Loading，设置false可去掉
      });
      if (result.code === "200" && result.rows) {
        this.signUpUserList = result.rows;
        if (this.signUpUserList.length > 4) {
          this.showBtn = true;
        }
      } else {
        this.$u.toast(result.msg);
      }
    },
    async getJobDetail() {
      let result = await this.$u.api.unifyMiniRestGet({
        systemid: "meily",
        url: "/expert/job/" + this.taskNumber,
        loading: true, // 默认发起请求会有一个全局的Loading，设置false可去掉
      });
      if (result.code === "200" && result.data) {
        this.form = result.data;
      } else {
        this.$u.toast(result.msg);
      }
    },
    //请求提交流程接口
    async signupDemand(action) {
      if (this.userList.length == 0 && !action) {
        this.$u.toast("请至少选择一个拍档通过！");
        return false;
      }
      const userInfo = uni.getStorageSync("userInfo");
      let result = await this.$u.api.unifyMiniRest({
        systemid: "meily",
        url: "/expert/job/preselect/user",
        userId: userInfo.userId,
        taskNum: this.taskNumber,
        users: this.userList,
        action: action,
        remark: this.form.remark,
      });
      if (result.code == "200") {
        uni.redirectTo({
          url:
            "/pages_private_scene1/ExpertCooperate/common/result?mode=31&taskNumber=" +
            this.taskNumber,
        });
      } else {
        this.$u.toast(result.msg);
      }
    },
  },
};
