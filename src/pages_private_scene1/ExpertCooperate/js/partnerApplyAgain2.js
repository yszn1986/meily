export default {
  data() {
    return {
      form: {
        subject: "",
        taskNum: "",
        budget: "",
        paymentType: "",
        deadline: "",
        details: "",
        createUser: "",
        remark: "",
        deliveryDate: "",
        bidPrice: "",
      },
      deliveryDateShow: false,
      subDateShow: false,
      subDateIndex: 0,
      timeParams: {
        year: true,
        month: true,
        day: true,
        hour: false,
        minute: false,
        second: false,
      },
      publisher: "",
      planJsonData: [],
      itemStyle: {},
      labelStyle: {},
      SelectedAttachments: {},
    };
  },
  onShow() {},
  onLoad(options) {
    this.taskNumber = options.taskNumber;
    if (this.taskNumber) {
      this.getJobDetail().then(() => {
        this.setPlan();
      });
    }
    this.itemStyle = {
      "border-bottom": "1px solid rgb(230, 230, 230)",
      padding: "0 8rpx",
      "font-size": "28rpx",
      "font-weight": "bold",
    };
    this.labelStyle = {
      "font-weight": "normal",
    };
  },
  methods: {
    async getJobDetail() {
      let result = await this.$u.api.unifyMiniRestGet({
        systemid: "meily",
        url: "/expert/job/" + this.taskNumber,
        loading: true, // 默认发起请求会有一个全局的Loading，设置false可去掉
      });
      if (result.code === "200" && result.data) {
        this.form = result.data;
        this.form.bidPrice = result.data.budget;
      } else {
        this.$u.toast(result.msg);
      }
    },
    showSelect(index) {
      this.subDateShow = true;
      this.subDateIndex= index-1;
    },
    setDeliveryDate(event) {
      this.form.deliveryDate = `${event.year}-${event.month}-${event.day}`;
      let prvDate = new Date(this.form.deadline);
      let curDate = new Date(this.form.deliveryDate);
      if (curDate.getTime() <= prvDate.getTime()) {
        this.$u.toast(
          "预计完成时间必须大于报名截止日期：" + this.form.deadline
        );
        this.form.deliveryDate = "";
      }
    },
    setPlanDate(event, index) {
      this.planJsonData[index].subDate = `${event.year}-${event.month}-${event.day}`;
      if (index == this.planJsonData.length - 1) {
        this.form.deliveryDate = this.planJsonData[index].subDate;
      }
      if (index >= 1) {
        const prvDate = new Date(this.planJsonData[index - 1].subDate);
        const curDate = new Date(this.planJsonData[index].subDate);
        if (!this.planJsonData[index - 1].subDate) {
          this.$u.toast("请先设置第" + index + "期交付日期!");
          this.planJsonData[index].subDate = "";
          return false;
        }
        if (curDate <= prvDate) {
          this.$u.toast("第" + (index + 1) + "期交付日期必须大于第" + index + "期！");
          this.planJsonData[index].subDate = "";
          return false;
        }
        if (curDate <= Date.parse(this.form.deadline)) {
          this.$u.toast("交付日期必须大于报名截止日期：" + this.form.deadline);
          this.planJsonData[index].subDate = "";
          return false;
        }
      } else {
        const  prvDate = new Date(this.form.deadline);
        const  curDate = new Date(this.planJsonData[index].subDate);
        if (curDate.getTime() <= prvDate.getTime()) {
          this.$u.toast("交付日期必须大于报名截止日期：" + this.form.deadline);
          this.planJsonData[index].subDate = "";
          return false;
        }
      }
    },
    //设置分期内容
    setPlan() {
      let array = this.form.paymentType.split(":");
      array.forEach((item, index) => {
        this.planJsonData.push({
          content: "",
          brokerage: "",
          subDate: "",
          sortNum: index + 1,
        });
      });
      this.setPlanBrokerage();
    },
    //计算每一期的佣金
    setPlanBrokerage() {
      if (parseInt(this.form.bidPrice) < parseInt(this.form.budget)) {
        this.form.bidPrice = "";
        this.$u.toast("不可低于项目预算：" + this.form.budget + "元");
        return false;
      }
      let array = this.form.paymentType.split(":");
      if (array.length > 1) {
        array.forEach((item, index) => {
          let sort = item.charAt(item.length - 1);
          sort = "0." + sort;
          this.planJsonData[index].brokerage = this.accMul(
            this.form.bidPrice,
            sort
          );
        });
      } else {
        this.planJsonData[0].brokerage = this.form.bidPrice;
      }
    },
    /**
     * 乘法计算
     * @param a
     * @param b
     * @returns {number}
     */
    accMul(a, b) {
      const getMul = (num) => num.toString().includes(".") ? num.toString().split(".")[1].length : 0;
      const mulA = 10 ** getMul(a);
      const mulB = 10 ** getMul(b);
      return (a * mulA * b * mulB) / (10 ** (getMul(a) + getMul(b)));
    },
    showDemand() {
      this.$u.route(
        "/pages_private_scene1/ExpertCooperate/jobInfo?taskNumber=" +
          this.taskNumber
      );
    },
    //请求提交流程接口
    async signupDemand() {
      for (let i = 0; i < this.planJsonData.length; i++) {
        if (!this.planJsonData[i].subDate) {
          this.$u.toast("请设置完成第" + (i + 1) + "期交付日期再提交计划！");
          return false;
        }
      }
      const userInfo = uni.getStorageSync("userInfo");
      //将计划写入报名表
      let result = await this.$u.api.unifyMiniRest({
        systemid: "meily",
        url: "/expert/job/reselect/partner",
        userId: userInfo.userId,
        nickName: userInfo.nickName,
        taskNum: this.taskNumber,
        deliveryDate: this.form.deliveryDate,
        workContent: this.planJsonData,
        bidPrice: this.form.bidPrice,
        loading: false, // 默认发起请求会有一个全局的Loading，设置false可去掉
      });
      if (result.code == "200") {
        uni.redirectTo({
          url:
            "/pages_private_scene1/ExpertCooperate/common/result?mode=4&taskNumber=" +
            this.taskNumber,
        });
      } else {
        this.$u.toast(result.msg);
      }
    },
  },
};
