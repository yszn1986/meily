export default {
  data() {
    return {
      form: {
        subject: "", //工作主题
        taskNum: "", //任务编号
        details: "", //需求描述
        budget: "", //项目预算
        paymentType: "", //付款方式
        deadline: "", //截止日期
        keywords: "", //关键字
        serviceRate: "0.3", //服务费率
        serviceFee: "", //服务费=30%保证金
        cashPledge: "", //保证金=10%项目预算
        createUser: "", //用户id
      },
      taskNumber: "",
      collections: [],
      ResumeFiles: [],
    };
  },

  onLoad(options) {
    this.taskNumber = options.taskNumber;
    if (this.taskNumber) {
      this.getJobDetail();
      this.getFiles(this.taskNumber);
    }
    let that = this;
    uni.getStorage({
      key: "collections",
      success: function (res) {
        that.collections = res.data;
      },
    });
  },

  methods: {
    async getJobDetail() {
      let result = await this.$u.api.unifyMiniRestGet({
        systemid: "meily",
        url: "/expert/job/" + this.taskNumber,
        loading: true, // 默认发起请求会有一个全局的Loading，设置false可去掉
      });
      if (result.code === "200" && result.data) {
        this.form = result.data;
      } else {
        this.$u.toast(result.msg);
      }
    },
    /**
     * 报名大厅需求
     */
    async signupHomeDemand() {
      //判断用户的登录状态
      let loginRes = this.checkLogin();
      if (loginRes) {
        let userInfo = uni.getStorageSync("userInfo");
        let result = await this.$u.api.unifyMiniRest({
          systemid: "meily",
          url: "/expert/job/grab",
          taskNum: this.form.taskNum,
          createUser: userInfo.userId,
          loading: true, // 默认发起请求会有一个全局的Loading，设置false可去掉
        });
        if (result.code == 200) {
          //抢工作机会成功
          this.$u.toast(result.msg);
          setTimeout(() => {
            uni.reLaunch({ url: result.url });
          }, 1500);
        } else {
          //抢工作机会失败
          this.$u.toast(result.msg);
        }
      }
    },
    /**
     * 收藏
     */
    async Collection() {
      let that = this;
      let orunid = that.docUnid;
      if (that.collections.indexOf(orunid) > -1) {
        let index = that.collections.indexOf(orunid);
        if (index > -1) {
          that.collections.splice(index, 1);
          uni.setStorage({
            key: "collections", // 认证流程表单字段与值
            data: that.collections,
            success: function () {},
          });
        }
        that.$u.toast("已取消收藏");
      } else {
        that.collections.push(orunid);
        uni.setStorage({
          key: "collections", // 认证流程表单字段与值
          data: that.collections,
          success: function () {},
        });
        that.$u.toast("收藏成功");
      }
    },
    async getFiles(orunid) {
      if (orunid) {
        const userInfo = uni.getStorageSync("userInfo");
        let result = await this.$u.api.unifyRequest({
          userId: userInfo.userId,
          nickName: userInfo.nickName,
          appid: "FILE",
          wf_num: "R_FILE_BL002",
          orunid: orunid,
          fieldName: userInfo.userId,
          loading: false, // 默认发起请求会有一个全局的Loading，设置false可去掉
        });
        if (result.code == 200) {
          this.ResumeFiles = result.data;
        }
      }
    },
    /**
     * 复制
     */
    async copyDemand() {
      this.$u.route(
        "/pages_private_scene1/ExpertCooperate/publishJob?taskNumber=" +
          this.taskNumber
      );
    },
    guid() {
      return (
        this.S4() +
        this.S4() +
        this.S4() +
        this.S4() +
        this.S4() +
        this.S4() +
        this.S4() +
        this.S4()
      );
    },

    S4() {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    },
  },
};
