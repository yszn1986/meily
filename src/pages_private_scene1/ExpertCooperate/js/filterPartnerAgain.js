export default {
  data() {
    return {
      form: {
        subject: "",
        taskNum: "",
        budget: "",
        paymentType: "",
        deadline: "",
      },
      userList: "",
      signUpUserList: [],
      taskNumber: "",
      passApply: false,
      refuseApply: false,
      showBtn: false,
    };
  },

  onLoad(options) {
    this.taskNumber = options.taskNumber;
    this.getJobDetail();
    this.getSignUpUserList();
  },

  methods: {
    showTips(item) {
      this.$u.route(
        "/pages_private_scene1/ExpertCooperate/partnerWorkPlan?userId=" +
          item.userId +
          "&taskNumber=" +
          this.taskNumber
      );
    },
    checkboxChange(e) {
      this.userSet = this.userSet || new Set();
      if (e.value) {
        this.userSet.add(e.name);
      } else {
        this.userSet.delete(e.name);
      }
      this.userList = Array.from(this.userSet).join(",");
      console.log(this.userList);
    },
    async getJobDetail() {
      let result = await this.$u.api.unifyMiniRestGet({
        systemid: "meily",
        url: "/expert/job/" + this.taskNumber,
        loading: true, // 默认发起请求会有一个全局的Loading，设置false可去掉
      });
      if (result.code === "200" && result.data) {
        this.form = result.data;
      } else {
        this.$u.toast(result.msg);
      }
    },
    //获取报名的最佳拍档
    async getSignUpUserList() {
      let result = await this.$u.api.unifyMiniRest({
        systemid: "meily",
        url: "/expert/signup/list",
        userId: uni.getStorageSync("userInfo").userId,
        taskNum: this.taskNumber, // 附件唯一id
        status: "待复选",
        loading: false, // 默认发起请求会有一个全局的Loading，设置false可去掉
      });
      if (result.code === "200" && result.rows) {
        this.signUpUserList = result.rows;
        if (this.signUpUserList.length > 4) {
          this.showBtn = true;
        }
      } else {
        this.$u.toast(result.msg);
      }
    },

    //请求提交流程接口
    async signupDemand(action) {
      if (action == "reject") {
        const userInfo = uni.getStorageSync("userInfo");
        let result = await this.$u.api.unifyMiniRest({
          systemid: "meily",
          url: "/expert/job/reselect/user",
          userId: userInfo.userId,
          taskNum: this.taskNumber,
          action: action,
        });
        if (result.code == "200") {
          uni.redirectTo({
            url:
                "/pages_private_scene1/ExpertCooperate/common/result?mode=52&taskNumber=" +
                this.taskNumber,
          });
        } else {
          this.$u.toast(result.msg);
        }
        return false;
      }
      if (this.userList.length == 0 ) {
        this.$u.toast("请选择拍档通过！");
        return false;
      }
      if (this.userList.split(",").length > 2) {
        this.$u.toast("只能通过一位最佳拍档！");
        return false;
      }
      let params = {
        paymentType: this.form.paymentType,
        taskNumber: this.taskNumber,
        userId: this.userList,
        action: action,
      };
      let url = "/pages_private_scene1/ExpertCooperate/modifyWorkPlan";
      this.$u.route(url, params);
    },
  },
};
