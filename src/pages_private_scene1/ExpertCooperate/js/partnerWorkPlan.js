export default {
  data() {
    return {
      form: {
        subject: "",
        workContent: "",
        deliveryDate: "",
        bidPrice: "",
        nickName: "",
        finalPrice: "",
      },
      taskNumber: "",
      pubPlan: false,
      userId: "",
      planJsonData: [],
      itemStyle: {},
      labelStyle: {},
    };
  },
  onShow() {},
  onLoad(options) {
    this.taskNumber = options.taskNumber;
    this.userId = options.userId;
    this.pubPlan = options.pubPlan;
    if (this.taskNumber && this.userId && !this.pubPlan) {
      this.getJobDetail().then(() => {
        this.planJsonData = JSON.parse(this.form.workContent);
        this.form.subject = options.subject;
      });
    }
    if (this.taskNumber && this.userId && this.pubPlan) {
      this.getJobPlan().then(() => {
        this.planJsonData = JSON.parse(this.form.workContent);
      });
    }

    this.itemStyle = {
      "border-bottom": "1px solid rgb(230, 230, 230)",
      padding: "0 8rpx",
      "font-size": "28rpx",
      "font-weight": "bold",
    };
    this.labelStyle = {
      "font-weight": "normal",
    };
  },
  methods: {
    async getJobPlan() {
      let result = await this.$u.api.unifyMiniRestGet({
        systemid: "meily",
        url: "/expert/job/"+ this.taskNumber,
        loading: false, // 默认发起请求会有一个全局的Loading，设置false可去掉
      });
      if (result.code === "200" && result.data) {
        this.form = result.data;
      } else {
        this.$u.toast(result.msg);
      }
      this.$nextTick(() => {
        this.$refs.collapseView.init();
      });
    },
    async getJobDetail() {
      let result = await this.$u.api.unifyMiniRest({
        systemid: "meily",
        url: "/expert/signup/detail",
        taskNum: this.taskNumber,
        userId: this.userId,
        loading: false, // 默认发起请求会有一个全局的Loading，设置false可去掉
      });
      if (result.code === "200" && result.data) {
        this.form = result.data;
      } else {
        this.$u.toast(result.msg);
      }
      this.$nextTick(() => {
        this.$refs.collapseView.init();
      });
    },
    autoHeight() {
      this.$nextTick(() => {
        this.$refs.collapseView.init();
      });
    },
    showDemand() {
      this.$u.route(
        "/pages_private_scene1/ExpertCooperate/jobInfo?taskNumber=" +
          this.taskNumber
      );
    },
    back() {
      this.goBack();
    },
  },
};
