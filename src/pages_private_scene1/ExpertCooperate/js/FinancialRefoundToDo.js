export default {
  data() {
    return {
      form: {
        Subject: "",
        TaskNum: "",
        Budget: "",
        publisher: "",
        RefundFees: "",
        ExpenseDetails: "",
      },
      WF_DocUnid: "",
      WF_Processid: "",
      WF_Taskid: "",
      WF_NextUserList: "",
      WF_NextNodeid: "",
      publisher: "",
      isClick: false, // 提交按钮是否可点击，默认可点击
    };
  },

  onLoad(options) {
    this.WF_DocUnid = options.docUnid;
    if (this.WF_DocUnid) {
      this.initFormData();
      this.getDemandDetail();
    }
  },

  methods: {
    async initFormData() {
      // 显示 loading 提示框
      uni.showLoading({
        mask: true,
      });

      // 设置按钮为不可点击
      this.isClick = true;

      const userInfo = uni.getStorageSync("userInfo");
      let processId = "19b255900d3970401e09a3b0524dbbdd2988";
      let result = await this.$u.api.unifyRequest({
        appid: "S017",
        wf_num: "R_S017_BH02",
        processId: processId,
        docUnid: this.WF_DocUnid,
        userId: userInfo.userId,
        loading: false, // 默认发起请求会有一个全局的Loading，设置false可去掉
      });
      if (result.data) {
        this.WF_DocUnid = result.data.docUnid;
        this.WF_Processid = result.data.processid;
        this.WF_Taskid = result.data.taskid;
        this.WF_NextNodeid = result.data.nextNodeList[0].nextNodeid;
        this.publisher = result.data.applyUser;

        uni.hideLoading();
        // 设置按钮为可点击
        this.isClick = false;
      } else {
        uni.hideLoading();
        // 设置按钮为可点击
        this.isClick = false;
      }
    },

    async getDemandDetail() {
      // 显示 loading 提示框
      uni.showLoading({
        mask: true,
      });

      // 设置按钮为不可点击
      this.isClick = true;

      let result = await this.$u.api.unifyRequest({
        appid: "ExpertCooperate",
        wf_num: "R_ExpertCooperate_BY001",
        userId: uni.getStorageSync("userInfo").userId,
        nickName: uni.getStorageSync("userInfo").nickName,
        docUnid: this.WF_DocUnid, // 附件唯一id
        loading: false, // 默认发起请求会有一个全局的Loading，设置false可去掉
      });
      if (result.code === "200" && result.data) {
        this.form = result.data;
        uni.hideLoading();
        // 设置按钮为可点击
        this.isClick = false;
      } else {
        this.$u.toast(result.msg);
        uni.hideLoading();
        // 设置按钮为可点击
        this.isClick = false;
      }
    },

    //请求提交流程接口
    async gotoNextNode() {
      // 显示 loading 提示框
      uni.showLoading({
        mask: true,
      });

      // 设置按钮为不可点击
      this.isClick = true;

      const userInfo = uni.getStorageSync("userInfo");
      let result = await this.$u.api.runProcess({
        WF_DocUnid: this.WF_DocUnid,
        WF_Processid: this.WF_Processid,
        userId: userInfo.userId,
        WF_Taskid: this.WF_Taskid,
        WF_NextUserList: this.publisher,
        WF_NextNodeid: this.WF_NextNodeid,
      });
      result = result.replace(/'/g, '"');
      let jsonData = JSON.parse(result.trim());
      if (jsonData.Status == "ok") {
        uni.redirectTo({
          url: "/pages_private_scene1/ExpertCooperate/common/result?mode=8",
        });
        uni.hideLoading();
        // 设置按钮为可点击
        this.isClick = false;
      } else {
        this.$u.toast(jsonData.msg);
        uni.hideLoading();
        // 设置按钮为可点击
        this.isClick = false;
      }
    },
  },
};
