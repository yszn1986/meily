/*
 * @Author: likang xie
 * @Date: 2020-09-28 16:58:01
 * @Purpose: http接口集中管理
 */

let install = (Vue, vm) => {
  // 登录
  let login = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/login`, params);

  //获取需求列表
  let getDemandList = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getDemandList`, params);

  //根据WF_OrUnid获取需求详情
  let demandDetail = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/demandDetail`, params);

  //获取拍档列表
  let getpartnerList = (params) => vm.$u.post(`/rest/ws/TAppletsInterf/findPartner`, params);

  //根据用户openid获取用户详细资料
  let getUserInfo = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getUserInfo`, params);

  //根据userId获取用户发布过的需求列表
  let getPublishDemands = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/user/publishDemands`, params);

  //初始化表单数据
  let getFormInitData = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getFormInitData`, params);

  //提交流程
  // let runProcess = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/runProcess`, params);

  //获取需求匹配列表
  let getDemandListByUserId = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getDemandListByUserId`, params);

  //报名需求
  let signupDemand = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/signupDemand`, params);

  //修改用户昵称
  let resetNickName = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/resetNickName`, params);

  //根据用户id查找注册流程，提供用户是否注册判断依据
  let getUserRegistedInfo = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getUserRegistedInfo`, params);

  //根据docUnid获取找人流程需求详情
  let getFindPartnerProcessDemadInfo = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getFindPartnerProcessDemadInfo`, params);

  //标签匹配用户
  let matchUser = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/matchUser`, params);

  // 初始化自定义标签（保存到数据库||取自定义标签）
  let initTagList = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/initTagList`, params);

  //获取perpartner_user信息
  let getPerpartnerUser = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getPerpartnerUser`, params);

  //完善个人标签
  let submitLabel = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/submitLabel`, params);

  //获取报名最佳拍档信息
  let getSignUpUsersInfo = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getSignUpUsersInfo`, params);

  //获取最佳拍档报名需求
  let getPartnerApplyDemandInfo = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getPartnerApplyDemandInfo`, params);

  //获取用户案例
  let getUserCaseList = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getUserCaseList`, params);

  //判断用户是否注册 流程4
  let userIsRegister = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/userIsRegister`, params);

  // 获取认证流程信息 流程4
  let getAuthenticationInfo = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getAuthenticationInfo`, params);

  // 更新认证标志/认证流程ID/专业技能/兴趣爱好 流程4
  let updatePerpartnerInfo = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/updatePerpartnerInfo`, params);

  // 认证流程修改昵称 流程4
  let updateNewName = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/updateNewName`, params);

  // 用户完善个人资料
  let improveUserInfo = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/improveUserInfo`, params);

  //获取个人资料
  let getImproveUserInfo = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getImproveUserInfo`, params);

  //获取岗位类别
  let getJobCategory = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getJobCategory`, params);

  //获取人才储备流程表单数据
  let getTalentFormData = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getTalentFormData`, params);

//获取首页推荐的最佳拍档列表
  let getRecommendPartners = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getRecommendPartners`, params);

  // 流程七(交付流程)获取流程六需求数据
  let getDemandInformation = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getDemandInformation`, params);

  // 获取流程七待办数据
  let getDeliveryProcessToDo = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getDeliveryProcessToDo`, params);

  //小程序需求列表匹配外的抢工作接口
  let seizeJob = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/seizeJob`, params);

  // 修改状态
  let updateTag = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/updateTag`, params);

  // 【平台发放款项】节点判断路由
  let determineRoute = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/determineRoute`, params);

  //找人流程，获取报名总人数
  let getTotalApplyPartner = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getTotalApplyPartner`, params);

  //根据WF_OrUnid获取需求详情
  let getDemandDetail = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getDemandDetail`, params);

  //根据文档ID获取通过复选的最佳拍档Userid
  let getBestPartnerByDocunid = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getBestPartnerByDocunid`, params);

  //确认支付
  let confirmPay = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/confirmPay`, params);

  // 交付流程付款后修改付款状态
  let updatePaymentStatus = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/updatePaymentStatus`, params);

  // 获取交付进度数据
  let getDeliveryScheduleInfoByDocunid = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getDeliveryScheduleInfoByDocunid`, params);

  // 用户付款后发送公众号通知
  let UserPaymentNotification = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/UserPaymentNotification`, params);
  
  //获取通知列表接口
  let getNotificationList = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getNotificationList`, params);
  
  //获取通知详情接口
  let getNotification = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getNotification`, params);
  
  //获取全部需求列表
  let getAllDemand = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getAllDemand`, params);
  
  //获取个人待办列表
  let getToDoList = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getToDoList`, params);
  
  //获取资源列表
  let getPartnerResourceList = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getPartnerResourceList`, params);
  
  //判断用户是否有待办
  let getUserToDoRecord = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getUserToDoRecord`, params);

  // 根据文档ID获取需求信息【交付流程】
  let getDemandInfoByDocUnid = (params) => vm.$u.post(`/rest/ws/TAppletsInterf/getDemandInfoByDocUnid`, params);

  // 判断项目款是否支付
  let projectAmountIsPay = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/projectAmountIsPay`, params);
  
  //收益接口--我的机会
  let getAllChancePrice = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getAllChancePrice`, params);
  
  //收益接口--我的收入
  let getAllJobPrice = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getAllJobPrice`, params);
  
  //收益接口--我的需求
  let getAllDemandPrice = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getAllDemandPrice`, params);
  
  //收益接口--我的支出
  let getAllDemandPayPrice = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getAllDemandPayPrice`, params);
  
  //积分接口--用户总积分
  let getUserTotalScore = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getUserTotalScore`, params);
  
  //积分接口--用户积分明细记录
  let getUserScoreList = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getUserScoreList`, params);

  //获取用户头像接口
  let getUseravatarUrl = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getUseravatarUrl`, params);
  
  //设置通知已读
  let setNoticeAlreadyRead = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/setNoticeAlreadyRead`, params);
  
  //获取需求列表接口--根据关键字搜索
  let getDemandsByKeyword = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getDemandsByKeyword`, params);
  
  //保存建议反馈接口
  let saveSuggestion = (params) => vm.$u.post(`/rest/ws/TAppletsInterf/saveSuggestion`, params);

  //获取当前项目进度占比
  let getProjectProgressRatio = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getProjectProgressRatio`, params);
  
  // 获取角色成员
  let getRoleMembersByRoleID = (params) => vm.$u.get(`/rest/ws/TAppletsInterf/getRoleMembersByRoleID`, params);
  
  //所有接口通用
  let unifyRequest = (params) => vm.$u.post(`/minrest`, params);
  
  let runProcess = (params) => vm.$u.post(`/minrest/runProcess`, params);
  
  //globalData.js配置的Remote_Config接口通用（post请求）
  let unifyMiniRest = (params) => vm.$u.post(params.url, params);
  //globalData.js配置的Remote_Config接口通用（get请求）
  let unifyMiniRestGet = (params) => vm.$u.get(params.url, params);
  
  vm.$u.api = {
    login,
    getDemandList,
    demandDetail,
    getpartnerList,
    getUserInfo,
    getPublishDemands,
    getFormInitData,
    runProcess,
    getDemandListByUserId,
    signupDemand,
    resetNickName,
    getUserRegistedInfo,
    getFindPartnerProcessDemadInfo,
    matchUser,
    initTagList,
    getPerpartnerUser,
    submitLabel,
    getSignUpUsersInfo,
    getPartnerApplyDemandInfo,
    getUserCaseList,
    userIsRegister,
    getAuthenticationInfo,
    updateNewName,
    updatePerpartnerInfo,
    improveUserInfo,
    getImproveUserInfo,
    getJobCategory,
    getTalentFormData,
    getRecommendPartners,
    getDemandInformation,
    getDeliveryProcessToDo,
    seizeJob,
    updateTag,
    determineRoute,
    getTotalApplyPartner,
    getDemandDetail,
    getBestPartnerByDocunid,
    confirmPay,
    updatePaymentStatus,
    getDeliveryScheduleInfoByDocunid,
    UserPaymentNotification,
    getNotificationList,
    getNotification,
    getAllDemand,
    getToDoList,
    getPartnerResourceList,
    getUserToDoRecord,
    getDemandInfoByDocUnid,
    projectAmountIsPay,
    getAllChancePrice,
    getAllJobPrice,
    getAllDemandPrice,
    getAllDemandPayPrice,
    getUserTotalScore,
    getUserScoreList,
    getUseravatarUrl,
    setNoticeAlreadyRead,
    getDemandsByKeyword,
    saveSuggestion,
    getProjectProgressRatio,
    getRoleMembersByRoleID,
	unifyRequest,
	unifyMiniRest,
	unifyMiniRestGet
  };
};

export default {
  install,
};
