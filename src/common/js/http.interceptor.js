/*
 * @Author: likang xie
 * @Date: 2020-09-28 16:58:01
 * @Purpose: http请求拦截器
 */

import globalData from "@/globalData";

let config = {
  // #ifdef H5
  // baseUrl: "/api",
  // #endif

  // #ifndef H5
  // baseUrl: globalData.remote,
  // #endif
  originalData: true, // 是否在拦截器中返回服务端的原始数据
  loadingText: "请稍后...", // 请求loading中的文字提示
  loadingMask: true, // 展示loading的时候，是否给一个透明的蒙层，防止触摸穿透
  loadingTime: 0, // 在此时间内，请求还没回来的话，就显示加载中动画，单位ms
};

let needLoginCodes = [401]; // 需要重新登录的状态码

let install = (Vue, vm) => {
  // 全局配置
  Vue.prototype.$u.http.setConfig(config);

  // 请求拦截
  Vue.prototype.$u.http.interceptor.request = (config) => {
	let contentType = "application/json"
	//是否需要设置自定义请求头
	let content_type = config.data.contentType
	if(content_type!=='undefined' && content_type!=="" && content_type!=null){
		contentType = content_type
	}
	let remote_config = globalData.Remote_Config
	let access_url = globalData.default_remote
	for(let i in remote_config){
		if(config.data.systemid == remote_config[i].system){//拦截请求，访问配置系统的地址
			access_url = remote_config[i].remote
		}
	}
	Vue.prototype.$u.http.setConfig({
		baseUrl: access_url
	});
	//add 2021.7.8 start
	const userInfo = uni.getStorageSync('userInfo')
	if(config.url == '/minrest/runProcess'){
		config.data.appid = 'S017'
		config.data.wf_num = 'R_S017_BH01'
		config.data.userId = userInfo.userId
		config.data.WF_Action = 'EndUserTask'
	}
	//add 2021.7.8 end
	
    let { loading = true } = config.data;

    Vue.prototype.$u.http.setConfig({
      showLoading: loading,
      header: {
        "Content-Type": contentType,
		// "Content-Type": "application/x-www-form-urlencoded;charset=utf-8",
        accessToken: uni.getStorageSync("access_token") || "",
        "sysid": "SysAdmin",
        "syspwd": "pass",
        "userId": userInfo.userId
      },
    });
	
	//重新封装请求数据,去除不需要的参数
	if(config.data.systemid != undefined){
		delete config.data.systemid
		if(config.data.url != undefined){
			delete config.data.url
		}
		if(config.data.contentType != undefined){
			delete config.data.contentType
		}
	}

    delete config.data.loading;

    // #ifdef MP-WEIXIN
    console.log("请求信息：", config);
    // #endif

    return config;
  };

  // 响应拦截
  Vue.prototype.$u.http.interceptor.response = (res) => {
    // #ifdef MP-WEIXIN
    console.log("响应信息：", res);
    // #endif

    let { code, message } = res.data;

    if (needLoginCodes.includes(code)) {
      //vm.$u.toast(message || "验证失败，请重新登录");
    } else if (code != 200) {
      //vm.$u.toast(message || "请求失败，请重试");
    }

    return res.data;
  };
};

export default {
  install,
};
