/*
 * @Author: zhaohui yuan
 * @Date: 2022-12-10 16:31:21
 * @Purpose: 程序主入口
 */

import Vue from "vue";
import App from "./App";
import store from "@/store";
import basicMixin from "@/mixins/basic";
import uView from "uview-ui";
import httpInterceptor from "@/common/js/http.interceptor";
import httpApi from "@/common/js/http.api";
import ShareBtn from "@/components/ShareBtn";
import Upload from "@/components/Upload";
import Notice from "@/components/Notice";

import globalData from "@/globalData";


App.mpType = "app";
Vue.config.productionTip = false;

let app = new Vue({
  ...App,
  store,
});

Vue.mixin(basicMixin);
Vue.use(uView);
Vue.use(httpInterceptor, app);
Vue.use(httpApi, app);

Vue.component("ShareBtn", ShareBtn);
Vue.component("Upload", Upload);
Vue.component("Notice", Notice);

Vue.prototype.checkLogin = function(isBack){
	const userInfo = uni.getStorageSync('userInfo')
	if(userInfo === ''){ // 本地没有openid表示未登录
		if(isBack == 1){
			this.$u.route('/pages_tabbar_contact/common_contact/wechatLogin?isBack=1')
		}else{
			uni.reLaunch({url:'/pages_tabbar_contact/common_contact/wechatLogin'})
		}
		return false
	}else{
		let version = userInfo.version
		if(version != globalData.version){
			uni.removeStorageSync('userInfo')
			uni.reLaunch({url:'/pages_tabbar_contact/common_contact/wechatLogin'})
			return false
		}
	}
	return true
}

//小程序更新版本重启应用
const updateManager = wx.getUpdateManager()
updateManager.onCheckForUpdate(function (res) {
  // 请求完新版本信息的回调
  console.log(res.hasUpdate)
})
updateManager.onUpdateReady(function () {
  wx.showModal({
    title: '更新提示',
    content: '新版本已经准备好，是否重启应用？',
    success: function (res) {
      if (res.confirm) {
        // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
        updateManager.applyUpdate()
      }
    }
  })
})
updateManager.onUpdateFailed(function () {
  // 新版本下载失败
})

app.$mount();

wx.cloud.init({
  // env: "devcloud-9gs1ejx73a92aa14" // 当前使用的测试环境
  env: "cloud1-2gh7k5ml91ffca0d" //生产环境
});