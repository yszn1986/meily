// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})

// 这里是云函数包的内容
exports.main = async (event, context) => {
  const res = await cloud.cloudPay.unifiedOrder({
    "body" : event.name, // 商品名称，需求名称+订金（1/n期项目验收款）
    "outTradeNo" : event.orderno, // 订单编号，需要保持统一性 需求编号 + new Date().getTime()
    "spbillCreateIp" : "127.0.0.1", 
    "subMchId" : "1606758025", // 商户号固定
    "totalFee" : event.price, // 支付金额，以分为单位的整数
    "envId": "devcloud-9gs1ejx73a92aa14", // 云开发环境
    "functionName": "yunpayCallback" // 回调云函数只要支付成功就会执行这里定义的云函数
  })
  return res
}
