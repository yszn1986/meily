// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})

// 日期格式化的方法
Date.prototype.Format = function () {
  const thisDay= new Date();
  const year = thisDay.getFullYear();       //年
  const month = thisDay.getMonth() + 1;     //月
  const day = thisDay.getDate();            //日
  const hh = thisDay.getHours();            //时
  const mm = thisDay.getMinutes();          //分
  const ss = thisDay.getSeconds();          //秒
  let timer = year + "-";
  if(month < 10)  
  timer += "0";
  timer += month + "-";
  if(day < 10)
  timer += "0";
  timer += day + " ";
  if(hh < 10)
  timer += "0";
    timer += hh + ":";
  if (mm < 10) 
  timer += '0'; 
  timer += mm + ":"; 
  if (ss < 10) 
  timer += '0'; 
  timer += ss; 
  return (timer); 
}

// 云函数入口函数
exports.main = async (event, context) => {
  // return await cloud.database().collection('demand_orders').where({
  //   orderno: event.outTradeNo
  // }).update({
  //   data: {
  //     orderstatus: 1
  //   }
  // }).then(res => {
  //     return res;
  //   }).catch(err =>{
  //     return err;
  // })
  return await cloud.database().collection('demand_orders').doc(event.outTradeNo).update({
    data: {
      orderstatus: 1,
      paytime: new Date().Format()
    }
  }).then(res => {
      return res;
    }).catch(err =>{
      return err;
  })
}